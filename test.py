import cv2
from image_resizer import resize

def test():
    try:
        resize("01.gif", 50, 50, "th_01.gif")
    except IOError as e:
        print e
    resize("image3.jpg", 600, 600, "600_600_donotkeep_fill.jpg", keepAspect = False)
    resize("image3.jpg", 600, 600, "600_600_fill.jpg", fill = True)
    resize("image3.jpg", 300, 600, "300_600_fill.jpg", fill = True)
    resize("image3.jpg", 600, 600, "600_600_fit.jpg", fill = False)
    resize("image3.jpg", 300, 600, "300_600_fit.jpg", fill = False)
    resize("image3.jpg", 1400, 900, "1400_900_fill.jpg", fill = True)
    
    # resizedImage = resize("image.png", 200, 200)
    # cv2.imwrite("resized_.png", resizedImage)

if __name__ == '__main__':
    test()
