from __future__ import division
import cv2

defaultOption = {
    'crop': False,
    'outputPath': None,
    'keepAspect': True,
    'fill': True
}

def resize(inputPath, targetWidth, targetHeight, options = defaultOption):
    image = cv2.imread(inputPath)
    if image is None:
        raise IOError("Cannot read file: %s" % inputPath)
    
    return resizeImage(image, targetWidth, targetHeight, options = options)
    
def resizeImage(srcImage, targetWidth, targetHeight, options = defaultOption):
    options = configOptions(options)
    
    if options['keepAspect']:
        resizeWidth, resizeHeight = evaluateTargetSize(srcImage, targetWidth, targetHeight, options['fill'])
    
    resultImage = cv2.resize(srcImage, (resizeWidth, resizeHeight))
    
    if options['fill'] and options['crop']:
        resizedImageHeight, resizedImageWidth = resultImage.shape[:2]
        centerX = resizedImageWidth / 2
        x = centerX - targetWidth / 2
        centerY = resizedImageHeight / 2
        y = centerY - targetHeight / 2
        resultImage = resultImage[y: targetHeight, x: targetWidth]
    
    if options['outputPath'] is not None:
        cv2.imwrite(options['outputPath'], resultImage)
    return resultImage
    
def evaluateTargetSize(image, targetWidth, targetHeight, fill = True):
    height, width = image.shape[:2]
    widthRatio = targetWidth / width
    heightRatio = targetHeight / height
    if fill:
        targetRatio = max(widthRatio, heightRatio)
    else:
        targetRatio = min(widthRatio, heightRatio)
    
    return (int(width * targetRatio), int(height * targetRatio))
    
def configOptions(options):
    copiedDefaultOptions = defaultOption.copy()
    
    for key in options:
        copiedDefaultOptions[key] = options[key]
    
    return copiedDefaultOptions
